module type t = 
sig 
        type 'elt queue

        type weight = int
        
        val empty : 'elt queue
        
        val is_empty : 'elt queue -> bool

        val add_with_priority : 'elt -> weight -> 'elt queue -> 'elt queue
        
        exception Empty

        val pop : 'elt queue -> 'elt * weight * 'elt queue
        
end


module MakeList : t