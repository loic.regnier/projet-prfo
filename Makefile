OCAMLC = ocamlc
BIN_TEST = bin/tests
BIN_P1 = bin/phase_1
BIN_P2 = bin/phase_2

all: depend test phase_1 phase_2

test : analyse.cmo priorityQueue.cmo graph.cmo path.cmo test.cmo | bin
	$(OCAMLC) -o $(BIN_TEST) $^

phase_1 : analyse.cmo priorityQueue.cmo graph.cmo path.cmo phase_1.cmo | bin
	$(OCAMLC) -o $(BIN_P1) $^

phase_2 : analyse.cmo graph.cmo phase_2.cmo | bin
	$(OCAMLC) -o $(BIN_P2) $^

%.cmo: %.ml
	$(OCAMLC) -c $<

%.cmi: %.mli
	$(OCAMLC) -c $<

bin:
	mkdir -p $@

clean:
	rm -f bin/tests
	rm -f bin/phase_1
	rm -f bin/phase_2
	rm -f *.cm[io]

depend:
	ocamldep *.mli *.ml > .depend

include .depend