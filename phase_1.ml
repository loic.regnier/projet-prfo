(*
  Module de graphe, pour représenter la base martienne   
*)
module G = Graph.Make(String)



(**
  @brief [file_to_graph file] renvoie renvoie le couple (base,(src,dst))
  où [base] est la plan de la base, [src] et [dst] sont respectivement le
  point de départ et d'arrivée de la personne dans la base.
  Ces éléments sont extraits du fichier [file], qui doit être de la structure
  adéquate.
  
  @param file : string

  @return G.t * (string * string)
*)
let file_to_graph file = 
  let (modules_list,(src,dst)) = Analyse.analyse_file_1 file in
  let rec sl_to_g_aux sl g = match sl with
    |[] -> g
    |(s1,s2,w)::sl' -> sl_to_g_aux sl' (G.add_weighted_line s1 s2 w g)
  in
  let string_list_to_graph sl = sl_to_g_aux sl (G.empty)
  in
  (string_list_to_graph modules_list,(src,dst))
;;


(*
  On récupère la fonction [bfs] du module Path.Path pour qui sert à déterminer
  le chemin le plus court entre 2 points d'un graphe.   
*)
module P = Path.Path(G)
open P

let shortest_path file =
  let base,(src,dst) = file_to_graph file in
  let path,time = bfs base src dst in
  Analyse.output_sol_1 time path
;;

(*
  Utilisation de la fonction [shortest_path] sur le fichier en argument   
*)
let n = Array.length (Sys.argv) 
  in
  if n<2 
  then Printf.fprintf stderr "phase_1.ml : 1 argument is expected.\n"
  else
    shortest_path (Sys.argv.(1))