(*
  Module servant à fournir la fonction 
  [bfs] : graph -> node -> node -> string list * int
  renvoyant le plus court chemin entre 2 noeuds du graphe en argument  
*)
module Path(G:Graph.t) =
  struct

    (* Récupération du module de priority queue *)
    module PQ = PriorityQueue.MakeList

    (**
      @brief [bfs_aux g prev todo dst] renvoie la map des prédécesseurs de chaque
      noeud de la base que l'on a eu à parcourir.
      
      @param g : graph
        le graphe de la base martienne
      @param prev : (node*int) NodeMap.t
        map qui à un noeud, associe son prédécesseur et le temps parcouru pour l'atteindre
      @param todo : node PriorityQueue 
        la file de priorité contenant les noeuds à explorer
      @param dst : node 
        le noeud d'arrivée

      @return (node*int) NodeMap.t
    *)
    let rec bfs_aux g prev todo dst = 
      if PQ.is_empty todo
      then raise Not_found
      else 
        let cur,path_w,todo' = PQ.pop todo in
        (*si on est arrivée à destination, on renvoie [prev]*)
        if cur = dst
        then prev
        
        else
          (*récupération des successeurs de [cur]*)
          let succs = G.succs cur g in
          (*calcul du nouveau [prev]*)
          let new_prev = 
            G.NodeSet.fold
              (
                fun next prev' -> 
                let n,w = next in
                let tot_w = path_w + w in
                try
                  let pred,w' = G.NodeMap.find n prev' in
                  (*si le poids déjà présent est + élevé, on 
                     l'actualise avec le nouveau qui est mieux*)
                  if w'>tot_w then G.NodeMap.add n (cur,tot_w) prev'
                  (*si non, on ne change rien*)
                  else prev'
                with Not_found -> (*on ajoute le prédécesseur*)
                  G.NodeMap.add n (cur,tot_w) prev' 
              )
              succs
              prev
          in 
          (*calcul du nouveau [todo]*)
          let new_todo =
            G.NodeSet.fold
            (
              fun next todo'' -> 
              let n,w = next in
              PQ.add_with_priority n (w+path_w) todo''
            )
            succs
            todo'
          in
          
          bfs_aux g new_prev new_todo dst

  
  (**
    @brief [rebuild_path prev_map src dst] renvoie la liste de noeuds
    correspondant au chemin allant de [src] à [dst] en suivant les transitions
        indiquées par la map [prev_map]

    @param prev_map : (node*int) NodeMap.t
    @param src : node
    @param dst : node

    @return node list
  *)
  let rec rebuild_path prev_map src dst = 
    if src = dst (*le chemin est terminé*)
    then [src]
    else 
      let new_dst,_ = G.NodeMap.find dst prev_map in
      dst::(
        rebuild_path
        prev_map
        src
        new_dst
      )

  (**
    @brief [bfs g src dst] renvoie le chemin le plus court entre [src] et [dst]
    ainsi que le temps de parcours.

    @param g : graph
    @param src : node
    @param dst : node

    @return node list * int
  *)
  let bfs g src dst = 
    (*calcul de la map des prédécesseurs*)
    let prev_map =
    bfs_aux 
    g
    G.NodeMap.empty
    (PQ.add_with_priority src 0 (PQ.empty))
    dst
    in
    (*récupération du temps de parcours*)
    let _,tot_w = G.NodeMap.find dst prev_map in
    (*résultat :*)
    (List.rev (rebuild_path prev_map src dst)),tot_w

  end
