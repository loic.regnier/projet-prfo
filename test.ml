Printf.printf "\n--------- TESTS PRIORITYQUEUE ---------\n\n";

Printf.printf "Implementation avec des listes :\n"
module PQList = PriorityQueue.MakeList
open PQList

let _ = let test = is_empty empty in if test
  then Printf.printf "empty_is_empty : OK\n"
  else Printf.printf "empty_is_empty : ERROR : returns -> %b, expected -> %b .\n" test true
;;

let _ = let v = is_empty (add_with_priority 3 1 empty) in 
        let test = v = false in 
if test
  then Printf.printf "empty_1 : OK\n"
  else Printf.printf "empty_1 : ERROR : returns %b, %b expected.\n" v false
;;

let _ = let e,w,pq = pop (add_with_priority 3 1 empty) in 
        let v = e=3 && w=1 && is_empty pq in
        let test = v = true in 
if test
  then Printf.printf "pop_1 : OK\n"
  else Printf.printf "pop_1 : ERROR : returns %b, %b expected.\n" v true
;;

let _ = let e,w,_ = pop (add_with_priority 3 10 (add_with_priority 2 1 (add_with_priority 1 10 empty))) in 
        let v = e=2 && w=1 in
        let test = v = true in 
if test
  then Printf.printf "pop_2 : OK\n"
  else Printf.printf "pop_2 : ERROR : returns %b, %b expected.\n" v true
;;

let _ = let v = try let _,_,_= pop empty in false
                with Empty -> true
        in 
        let test = v = true in 
if test
  then Printf.printf "pop_empty : OK\n"
  else Printf.printf "pop_empty : ERROR : returns %b, %b expected.\n" v true
;;




Printf.printf "\n------------ TESTS GRAPH ------------\n\n";

(*on realise les tests avec noeuds entiers*)
module G = Graph.Make(Int)
open G

let _ = let test = is_empty empty in if test
  then Printf.printf "empty_is_empty : OK\n"
  else Printf.printf "empty_is_empty : ERROR : returns -> %b, expected -> %b .\n" test true
;;


let _ = let v = is_empty (add_node 1 empty) in 
        let test = v = false in 
if test
  then Printf.printf "empty_2 : OK\n"
  else Printf.printf "empty_2 : ERROR : returns %b, %b expected.\n" v false
;;

let _ = let v = is_empty (add_weighted_line 1 2 5 empty) in 
        let test = v = false in 
if test
  then Printf.printf "empty_1 : OK\n"
  else Printf.printf "empty_1 : ERROR : returns %b, %b expected.\n" v false
;;


Printf.printf "\n------------- TESTS PATH -------------\n\n";

module GString = Graph.Make(String)
module P = Path.Path(GString)
open P
open GString

let rec sl_to_g_aux sl g = match sl with
|[] -> g
|(s1,s2,w)::sl' -> sl_to_g_aux sl' (add_weighted_line s1 s2 w g)

;;

let string_list_to_graph sl = sl_to_g_aux sl (GString.empty);;


let _ = let modules_list,(src,dst) = Analyse.analyse_file_1 "test_bases/ex_base_1" in
        let g_test = string_list_to_graph modules_list in
        let p,t = bfs g_test src dst in 
        let exp_string_list = ["A";"B"] and exp_time = 12 in
        let test = p = exp_string_list && t = exp_time in
if test
  then Printf.printf "test_base_1 : OK\n"
  else (Printf.printf "test_base_1 : ERROR\n    Returns :                           " ; 
      Analyse.output_sol_1 t p ;
      Printf.printf "    But this expression was expected :  ";
      Analyse.output_sol_1 exp_time exp_string_list)
;;

let _ = let modules_list,(src,dst) = Analyse.analyse_file_1 "test_bases/ex_base_2" in
        let g_test = string_list_to_graph modules_list in
        let p,t = bfs g_test src dst in 
        let exp_string_list = ["A";"B";"C"] and exp_time = 6 in
        let test = p = exp_string_list && t = exp_time in
if test
  then Printf.printf "test_base_2 : OK\n"
  else (Printf.printf "test_base_2 : ERROR\n    Returns :                           " ; 
      Analyse.output_sol_1 t p ;
      Printf.printf "    But this expression was expected :  ";
      Analyse.output_sol_1 exp_time exp_string_list)
;;

let _ = let modules_list,(src,dst) = Analyse.analyse_file_1 "test_bases/ex_base_3" in
        let g_test = string_list_to_graph modules_list in
        let p,t = bfs g_test src dst in 
        let exp_string_list = ["A";"C";"E"] and exp_time = 4 in        
        let test = p = exp_string_list && t = exp_time in
if test
  then Printf.printf "test_base_3 : OK\n"
  else (Printf.printf "test_base_3 : ERROR\n    Returns :                           " ; 
      Analyse.output_sol_1 t p ;
      Printf.printf "    But this expression was expected :  ";
      Analyse.output_sol_1 exp_time exp_string_list)
;;

let _ = let modules_list,(src,dst) = Analyse.analyse_file_1 "test_bases/ex_base_4" in
        let g_test = string_list_to_graph modules_list in
        let p,t = bfs g_test src dst in 
        let exp_string_list = ["A";"C";"E";"F"] and exp_time = 14 in
        let test = p = exp_string_list && t = exp_time in
if test
  then Printf.printf "test_base_4 : OK\n"
  else (Printf.printf "test_base_4 : ERROR\n    Returns :                           " ; 
      Analyse.output_sol_1 t p ;
      Printf.printf "    But this expression was expected :  ";
      Analyse.output_sol_1 exp_time exp_string_list)
;;
