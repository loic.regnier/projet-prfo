(*
  Module de graphe, pour représenter la base martienne.   
*)
module G = Graph.Make(String)

(*
  Map qui a des entiers pour clé. Utile pour la map de positions,
  et la map résultat. Dans ces 2 maps, les entiers représentent
  les personnes dans la base.   
*)
module M = Map.Make(Int)

(**
  [tunnel_compare t1 t2] compare 2 tunnels. La définition d'un tunnel
  est explicitée ci-dessous l.31-l.33
  
  @param t2 : string * string
  @param t2 : string * string

  @return int
*)
let tunnel_compare (s1,s1') (s2,s2') = 
  let c = String.compare s1 s2 in
  if c = 0 then String.compare s1' s2'
  else c 
 
(*
  Map de priorités, indique lors des priorités de passage dans un 
  tunnel entre 2 modules. La clé est une paire de modules (de type
  G.node, donc ici string). 
  Un passage entre 2 modules m1 et m2, où m1 < m2 selon String.compare,
  est désigné par la paire (m1,m2).
  (la paire (m2,m1) n'existe pas, on met le plus petit module à gauche)   
*)
module Prio = Map.Make(
    struct 
      type t = string * string
      let compare = tunnel_compare
    end
    );;


(**
  @brief [get_tunnel m1 m2] renvoie le tunnel entre [m1] et [m2]
  dans le sens défini précédemment : (m1',m2') où m1' = min m1 m2 et
  m2' = max m1 m2    

  @param m1 : string
  @param m2 : string

  @return string * string
*)
let get_tunnel m1 m2 = 
  if String.compare m1 m2 < 0 
  then m1,m2
  else m2,m1
;;
(**
  @brief [file_to_graph file] renvoie renvoie le couple (base,paths_list)
  où [base] est la plan de la base, et [paths_list] est la liste de
  chemins empruntés par les personnes dans la base.
  Ces éléments sont extraits du fichier [file] qui doit être de la 
  structure adéquate.
  
  @param file : string

  @return G.t * (string list list)
*)
let file_to_graph file = 
  let (modules_list,paths_list) = Analyse.analyse_file_2 file in
  let rec sl_to_g_aux sl g = match sl with
    |[] -> g
    |(s1,s2,w)::sl' -> sl_to_g_aux sl' (G.add_weighted_line s1 s2 w g)
  in
  let string_list_to_graph sl = sl_to_g_aux sl (G.empty)
  in
  (string_list_to_graph modules_list,paths_list)
;;

(**
  @brief [tunnel_list path] renvoie la liste des tunnels empruntés
  par le chemin [path]
  
  @param path : string list

  @return (string * string) list
*)
let rec tunnel_list path = 
  match path with
  |[n1;n2] ->  [get_tunnel n1 n2]
  |n1::n2::path' -> 
    (get_tunnel n1 n2)::(tunnel_list (n2::path'))
  |_ -> failwith "Wrong argument (tunnel_list)" 


(**
  @brief [max_potentiel_time base prioMap path] renvoie le temps potentiel
  maximal du chemin [path]. Le temps potentiel maximal est la somme de tous
  les poids des arêtes du chemin, mais chaque poids est multiplier par le
  nombre de personne convoitant le tunnel.
  
  @param base : graph
  @param prioMap : (int * bool * int) list * int Prio.t
  @param path : string list

  @return int
*)
let max_potential_time base prioMap path = 
  let tun_list = tunnel_list path in
  List.fold_left
  (
    fun tot_weight tun -> 
      let prio,_ = Prio.find tun prioMap in
      let coef = List.length prio in
      tot_weight + coef * (G.get_weight tun base)
  )
  0
  tun_list


(**
  @brief [path_after_tun tunnel path] renvoie le sous chemin correspondant
  au chemin à parcourir après avoir passé le tunnel [tunnel]

  @param tunnel : string * string
  @param path : string * list

  @return string * list
*)
let rec path_after_tun tunnel path =
  match path with
  |[n1;n2] -> []
  |n1::n2::path' ->
    (*si c'est le tunnel, on renvoie le reste du chemin*) 
    if tunnel_compare (get_tunnel n1 n2) tunnel = 0
    then n2::path'
    (*sinon on poursuit le parcours de la liste*)
    else path_after_tun tunnel (n2::path')
  |_-> failwith "Wrong argument (path_after_tun)"


(**
  @brief [priority_map base paths_list] renvoie la map des priorités de
  passage dans les tunnels.
  
  @warning les transitions de [paths_list] doivent être cohérentes avec les
  arête de [base]

  @param base : G.t
  @param paths_list : string list list

  @return int * blool * int list * int Prio.t

*)
let priority_map base paths_list = 
  let res,_ =
  (*on commence par ajouter toutes les personnes passant par les tunnels*)
  List.fold_left
  (
    fun (prioMap,person) path ->
      let tlist = tunnel_list path in
      let new_prioMap =
      List.fold_left
      (
        fun pmap tun ->
          try 
          let prio_list,travel_time = Prio.find tun pmap in
          Prio.add tun ((person,false,0)::prio_list,travel_time) pmap
          
          with
          |Not_found ->
            Prio.add tun ([(person,false,0)],G.get_weight tun base) pmap

          
      )
      prioMap
      tlist
      in
      (new_prioMap,person+1)

  )
  (Prio.empty,0)
  paths_list
  
  in
  
  (*on récupère la liste des tunnels utilisés*)
  let all_tun_list = 
    List.sort_uniq 
    tunnel_compare 
    (List.concat (List.map tunnel_list paths_list))
  in
  (*on établit l'ordre de priorité de passage dans chaque tunnel*)
  List.fold_left
  (
    fun prioMap tun ->
      let sort_function (p1,w1,t1) (p2,w2,t2) =
          let path1,path2 = List.nth paths_list p1,List.nth paths_list p2 in
          (*on les tris du plus gros poids au plus faible*)
          - compare (path_after_tun tun path1) (path_after_tun tun path2)
      in
      let prio_list,t = Prio.find tun prioMap in
      Prio.add tun ((List.sort sort_function prio_list),t) prioMap

  )
  res
  all_tun_list
;;
(**
  @brief [calc_res_aux paths_list prioMap posMap resMap] retourne le couple 
  (finalResMap,max_time) où [finalResMap] est la map qui à une personne de la base
  associe sa liste de chemins et sa liste des temps. max_time est le temps de la personne 
  arrivant en dernier à sa destination, c'est donc le temps total d'exécution
  des trajets.
  [prioMap], [posMap], [resMap] sont respectivement la map des priorités, 
  la map des positions, la map résultat.
  C'est la fonction auxiliaire de [calc_res].
  
  @param prioMap : (int list * int) Prio.t
      Map qui à une paire de module, associe l'ordre de priorité de passage
      sous la forme d'une int list, ainsi que le temps de traversée du tunnel.
  @param posMap : ((string * int), string list) M.t
      Map qui à une personne (:int) associe le couple 
      ((modul,time),path) : (string * int) * (string list)
      représentant la position avec sa minute d'arrivée, et le chemin restant à parcourir.
  @param resMap : (string list * int list) M.t
      Map qui à une personne (:int) associe le couple 
      (mod_list,time_list) : string list * int list

  @warning toutes les arêtes du graphe représentant la base martienne doivent
  être présentes dans [prioMap]
  @warning toutes les personnes de la base doivent être présentes dans
  [posMap] et [resMap]

  @return (string list * int list) M.t * int
*)
let rec calc_res_aux prioMap posMap resMap max_time =
  let (new_prioMap,new_posMap,new_resMap,new_max_time) =
  M.fold 
  (
    fun person ((modul,t),path) (prioMap,posMap,resMap,max_time) ->
      match path with
      |[] -> (*la personne est arrivée à destination, on la retire de la map*)
        let mod_list,time_list = M.find person resMap in
        prioMap,
        (M.remove person posMap),
        (M.add person (modul::mod_list,time_list) resMap),
        max_time
      
      |next_mod::next_path -> 
      (
        let tun = get_tunnel modul next_mod in
      (*on récupère l'ordre de priorité du tunnel*)
        match Prio.find tun prioMap with
        |((next_person,w,waiting_time)::rest,travel_time) ->
          (
          if next_person = person 
            (*c'est bien à son tour de passer*)
          then 
            (*calcul nouvelle position et ajout du résultat dans la map*)
            let mod_list,time_list = M.find person resMap in
            let new_res = (modul::mod_list),((t+waiting_time)::time_list) in
            let dst_time = t+waiting_time+travel_time in
            let new_pos = ((next_mod,dst_time),next_path) in
            (*on ajoute le temps d'attente de ceux qui ont attendus*)
            let new_rest = 
              List.map
              (fun (prsn,waiting,w_time) ->
                if waiting then 
                  let (_,t'),_ = M.find prsn posMap in
                    (prsn,waiting,w_time+(t-t')+travel_time)
                  else (prsn,waiting,w_time))
              rest
            in
            (Prio.add tun (new_rest,travel_time) prioMap),
            (M.add person new_pos posMap),
            (M.add person new_res resMap),
            (dst_time)
          
            (*ce n'est pas à son tour de passer
              on ne fait rien, le temps d'attente sera ajouté lors
              du passage de la personne prioritaire*)
          else
            let new_rest = 
              List.map
              (fun (prsn,waiting,w_time) ->
                if prsn=person then 
                    (prsn,true,w_time)
                  else (prsn,waiting,w_time))
              rest
            in
            (Prio.add tun (((next_person,w,waiting_time)::new_rest,travel_time)) prioMap),
            posMap,
            resMap,
            max_time
          )

        |_-> failwith "Wrong argument prioMap (calc_res_aux)" 
      )
  )
  posMap
  (prioMap,posMap,resMap,max_time)
  in
  if M.is_empty posMap 
    (*c'est terminé*)
    then new_resMap,new_max_time
    (*ce n'est pas terminé, on continue*)
    else calc_res_aux new_prioMap new_posMap new_resMap new_max_time

  ;;

(**
  @brief [calc_res prioMap] retourne le couple (finalResMap,max_time)
  où [finalResMap] est la map qui à une personne de la base associe sa liste de
  chemins et sa liste des temps. max_time est le temps de la personne 
  arrivant en dernier à sa destination, c'est donc le temps total d'exécution
  des trajets. 
  [prioMap] est la map des priorités.
  
  @param paths_list : string list list
  @param prioMap : (int list * int) Prio.t
      Map qui à une paire de module, associe l'ordre de priorité de passage
      sous la forme d'une int list.
    
  @return int M.t * int
*)
let calc_res paths_list prioMap =
  (* initialisation de posMap et resMap *)
  let posMap,resMap,_ = 
  List.fold_left
  (
    fun (posMap,resMap,person) path ->
      match path with
      |[] -> failwith "Empty path."
      |start::path' -> 
        (M.add person ((start,0),path') posMap), (*position de départ au temps 0*)
        (M.add person ([],[]) resMap), (*listes vides pour chaque personne*)
        (person+1) (*on passe à la personne suivante*)
  )
  (M.empty,M.empty,0)
  paths_list

  in
  let resMap,max_time = (*on récupère le résultat avec la fonction auxiliaire*)
    calc_res_aux prioMap posMap resMap 0
in resMap,max_time
  ;;
 

(**
  @brief [travel_management file] affiche, sur la sortie standard, les 
  différents chemins des voyageurs dans la base, avec le moment auquel
  ils traversent pour chaque transition ; ainsi que le temps du parcours
  le plus élevé.
  
  @param file : string

  @return unit
*)
open Printf
let travel_management file = 
  let (base,paths_list) = file_to_graph file in
  let prioMap = priority_map base paths_list in
  let (resMap,max_time) = calc_res paths_list prioMap in
  let res =
  M.fold
    (
      fun _ (mod_list,time_list) res_list ->
        (List.rev mod_list,List.rev time_list)::res_list
    )
    resMap
    []
      in
  Printf.printf "Durée totale des déplacements : %i\n" max_time;
  Analyse.output_sol_2 res 
;;


(*
  Utilisation de la fonction [travel_management] sur le fichier en argument   
*)
let n = Array.length (Sys.argv) 
  in
  if n<2 
  then Printf.fprintf stderr "phase_2.ml : Exactly 1 argument is expected.\n"
  else
    travel_management (Sys.argv.(1))