Compilation:

Pour compiler le projet, il suffit d'entrer "make" dans le terminal.
La compilation créer 3 exécutables dans le dossier bin : 
tests, phase_1 et phase_2.


Utilisation:

Pour lancer les tests :
    ./bin/tests

Pour lancer phase_1 et phase_2 :
Les 2 exécutables attendent un argument, étant un fichier contenant
le plan de la base ainsi d'autres informations spécifiques à chaque
phase, comme décrites dans le sujet.
Des fichiers de test sont disponibles dans le dossier test_bases.


Exemples d'utilisation :

./bin/tests
./bin/phase_1 test_bases/ex_base_1
./bin/phase_2 test_bases/ex_travels_1