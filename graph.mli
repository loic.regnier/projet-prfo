
module type t = 
sig
    type node

    type weight = int

    module NodeMap : Map.S with type key = node

    type graph

    val empty : graph

    val is_empty : graph -> bool

    val add_node : node -> graph -> graph

    val add_weighted_arrow : node -> node -> weight -> graph -> graph

    val add_arrow : node -> node -> graph -> graph

    val add_weighted_line : node -> node -> weight -> graph -> graph

    val add_line : node -> node -> graph -> graph

    module NodeSet : Set.S with type elt = node * weight

    val succs : node -> graph -> NodeSet.t

    val get_weight : node * node -> graph -> int

end

module Make(OrdT:Set.OrderedType) : t with type node = OrdT.t