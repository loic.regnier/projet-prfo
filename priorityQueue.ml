module type t = 
sig 
    (*
        ['elt queue] est le type d'une file de priorite
        Une file de priorite a un poids associe a chacun de ses elements
        L'element au somment de la file est celui de poids le plus faible   
    *)
    type 'elt queue
    
    (*
        Type du poids des elements d'une file de priorite
        On choisit le type int   
    *)
    type weight = int
    
    (*
        Valeur correspondant a la file vide   
    *)
    val empty : 'elt queue
        
    (**
        [is_empty pq] renvoie true si pq est la file vide, false sinon
        
        @param pq : 'elt queue
    
        @return bool
    *)
    val is_empty : 'elt queue -> bool

    (**
        @brief [add_with_priority e w pq] renvoie la file de priorite
        [pq] a laquelle l'element [e] a ete ajoute avec un poids [w] 

        @param e : 'elt l'element a ajouter dans la file
        @param w : weight le poids de l'element [e]
        @param pq : 'elt queue la file de priorite dans laquelle on souhaite
            ajouter [e]

        @return 'elt queue
    *)
    val add_with_priority : 'elt -> int -> 'elt queue -> 'elt queue
    
    (*
        Exception levee par la fonction [pop]   
    *)
    exception Empty

    (**
        @brief [pop pq] renvoie le premier element de la file, son poids,
        ainsi que la file de priorite sans cet element, sous la forme
        d'un triple de valeurs
        
        @param pq : 'elt queue file de priorite 

        @return 'elt * int * 'elt queue
    *)
    val pop : 'elt queue -> 'elt * int * 'elt queue
end

(*
    Le module MakeList implemente les PriorityQueue avec des listes
*)
module MakeList =
struct
    type 'elt queue = ('elt * int) list

    type weight = int

    let empty = []

    let is_empty q = q = []

    exception Empty
    
    let pop q = match q 
    with
    | [] -> raise Empty
    | (e,p)::q2 -> (e,p,q2)

    let rec add_with_priority e p q = 
        match q with
        | [] -> [(e,p)]
        | (e2,p2)::q2 ->
            if p < p2
            then (e,p)::(List.filter (fun (e',_) -> e'<>e) q) 
            else 
                (* si c'est le meme element, alors on ne fait rien *)
                if e=e2 then q  
                else (e2,p2)::add_with_priority e p q2
    
end

(*
test de merde:

module PQ = MakePQList

open PQ


let rec print_list l = match l with
|[] -> Printf.printf "[]\n"
|[(a,p)] -> Printf.printf "(%i,%i) ]\n" a p
|(a,p)::l2 -> Printf.printf "(%i,%i) ; " a p; print_list l2
;; 

let q = [];;

Printf.printf "vrai : %b\n" (is_empty q);;

let q = add_with_priority 1 1 q;; 
let q = add_with_priority 2 5 q;;
let q = add_with_priority 3 3 q ;;
let q = add_with_priority 3 7 q ;;
let q = add_with_priority 4 1 q ;;
Printf.printf "FE\n";;
print_list q;;

*)