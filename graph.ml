module type t = 
sig
    (*
      Type des noeuds du graph   
    *)
    type node

    (*
      Type du poids des arcs du graph
      On prend des entiers positifs : tout element de type weight
      est un entier positif ou nul 
    *)
    type weight = int

    (*
      Module d'une map dont les cles sont de type node
      On l'utilise pour definir le type graph   
    *)
    module NodeMap : Map.S with type key = node

    (*
      type graph = (weight NodeMap.t) NodeMap.t   
    *)
    type graph

    (*
      Valeur d'un graph vide   
    *)
    val empty : graph

    (**
      @brief [is_empty g] renvoie true si [g] est vide, false sinon

      @param g : graph

      @return bool
    *)
    val is_empty : graph -> bool

    (**
      @brief [add_node n g] ajoute le noeud [n] au graph [g] et renvoie
      le resultat
      
      @param n : node le noeud a ajouter
      @param g : graph 

      @return graph le graph [g] auquel on a ajoute le noeud [n]
    *)
    val add_node : node -> graph -> graph

    (**
      @brief [add_weighted_arrow n1 n2 w g] ajoute l'arc (n1,n2) de 
      poids [w] au graph [g] et renvoie le resultat

      @param n1 : node
      @param n2 : node
      @param w : weight entier >= 0
      @param g : graph

      @warning si [n1] et/ou [n2] ne sont pas dans le graph [g] alors
      ils sont ajoutes par la fonction
      @warning si un arc (n1,n2) existe deja, il est ignore et remplace
      par l'arc de poids [w]

      @return graph le graph [g] auquel on a ajoute l'arc (n1,n2) de
      poids [w]
    *)
    val add_weighted_arrow : node -> node -> weight -> graph -> graph

    (**
      @brief [add_arrow n1 n2 g] ajoute l'arc n1-->n2 de poids nul
      au graph [g] et renvoie le resultat
      
      @param n1 : node
      @param n2 : node
      @param g : graph

      @return graph le graph [g] auquel on a ajoute l'arc n1-->n2
      de poids nul
    *)
    val add_arrow : node -> node -> graph -> graph

    (**
      @brief [add_weighted_line n1 n2 w g] ajoute l'arete n1--n2 de 
      poids [w] au graph [g] et renvoie le graphe resultat

      @param n1 : node
      @param n2 : node
      @param w : weight entier >= 0
      @param g : graph

      @warning si [n1] et/ou [n2] ne sont pas dans le graph [g] alors
      ils sont ajoutes par la fonction
      @warning si un arc (n1,n2) existe deja, il est ignore et remplace
      par l'arc de poids [w]

      @return graph le graph [g] auquel on a ajoute l'arete n1--n2 de
      poids [w]
    *)
    val add_weighted_line : node -> node -> weight -> graph -> graph

    (**
      @brief [add_line n1 n2 g] ajoute l'arete n1--n2 de poids nul
      au graph [g] et renvoie le resultat
      
      @param n1 : node
      @param n2 : node
      @param g : graph

      @return graph le graph [g] auquel on a ajoute l'arete n1--n2
      de poids nul
    *)
    val add_line : node -> node -> graph -> graph
    (*
      Module d'ensemble de couples noeud * poids, utilise pour la
      valeur de retour de la fonction [succs]   
    *)
    module NodeSet : Set.S with type elt = node * weight

    (**
      @brief [succs n g] renvoie les successeurs du noeud [n] ainsi
      que le poids des arcs partant de [n], dans un ensemble de couples
      node * weight
      
      @param n : node
      @param g : graph

      @return NodeSet.t ensemble de couples (successeur de n , poids de l'arc)
    *)
    val succs : node -> graph -> NodeSet.t

    (**
      @brief [get_weight (n1,n2) g] renvoie le poids de l'arc n1 -> n2
      dans le graph [g]
      
      @param (n1,n2) : node * node
      @param g : graph

      @return int
    *)
    val get_weight : node * node -> graph -> int

end

module Make(OrdT:Set.OrderedType) =
struct

  exception Not_in_graph

  type weight = int

  type node = OrdT.t

  module NodeSet = Set.Make(
    struct 
      type t = node*weight
      let compare nw1 nw2 = 
        let (n1,_),(n2,_)=nw1,nw2 in OrdT.compare n1 n2
    end
    );;

  module NodeMap = Map.Make(OrdT);;

  module PQMap = Map.Make(OrdT)

  type graph = (weight NodeMap.t) NodeMap.t;;

  let empty = NodeMap.empty ;;

  let is_empty g = g = empty;; 

  let add_node n g = NodeMap.add n empty g ;;

  (* si un arc n1 ---w'---> n2 existe déjà, on le supprime et le remplace 
     par le nouveau*)
  let rec add_weighted_arrow n1 n2 w g = 
    try 
    let nmap = NodeMap.find n1 g in
    NodeMap.add n1 (NodeMap.add n2 w nmap) g
    with Not_found -> 
      let new_g = add_node n1 g in
      add_weighted_arrow n1 n2 w new_g
    ;;
    
    (*on considère qu'ajouter un arc c'est ajouter un arc de poids 0*)
  let add_arrow n1 n2 g = add_weighted_arrow n1 n2 0 g;;


  let add_weighted_line n1 n2 w g = 
    add_weighted_arrow n1 n2 w
        (add_weighted_arrow n2 n1 w g)
  ;;

  let add_line n1 n2 g =
    add_arrow n1 n2 (add_arrow n2 n1 g)
  ;;
  
  let succs n g = 
    try
      let nmap = NodeMap.find n g in
      NodeMap.fold 
      (fun n' w nset -> NodeSet.add (n',w) nset) 
      nmap
      NodeSet.empty
    with Not_found -> NodeSet.empty
  ;;

  let get_weight (n1,n2) g = 
    try 
    NodeMap.find n2 (NodeMap.find n1 g)
    with Not_found -> 
      failwith "Wrong argument (get_weight)"
    ;;

end 